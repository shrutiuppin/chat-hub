var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./app",
            routes: {
                "/node_modules": "node_modules"
            }
        },
        files: ["app/js/**/*.js","app/js/**/*.html","app/js/*.js","app/styles/*.css","app/index.html"]
    });
});

gulp.task('default', ['browser-sync']);

