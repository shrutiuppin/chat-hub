'use strict';

angular.module('chatRoom')
    .run(Run);

function Run(routerHelper) {
    routerHelper.configureStates(getChatRoomStates());
}

function getChatRoomStates() {
    return [
        {
            state: 'chatRoom',
            config: {
                controller: chatRoomController,
                templateUrl: 'js/chatRoom/chatRoom.html',
                url: '/chatRoom'
            }
        }
    ];
}