'use strict';
myChatApp.factory('chatData', chatData)
    .run(connect);

function connect(chatData) {
    chatData.connect('localhost', 8080);
}

function chatData($rootScope) {
    var ws;
    var service = {
        'connect': connect,
        'disconnect': disconnect,
        'send': send
    };
    return service;

    function connect(address, port) {
        if (!ws) {
            ws = new WebSocket("ws://" + address + ":" + port);
            console.dir(ws);
            ws.onopen = function () {
                console.log("opened");
            };

            ws.onmessage = function (data) {
                var data = JSON.parse(data.data);
                $rootScope.$broadcast(data.event, data.data);
            };

            ws.onerror = function (event) {
                console.log("There was an error connecting " + address + ":" + port);
            };
        }
    }

    function disconnect() {
        if (ws) {
            ws.close();
        }
    }

    function send(msg) {
        if (ws && ws.readyState == WebSocket.OPEN) {
            ws.send(JSON.stringify(msg));
        }
    }
}