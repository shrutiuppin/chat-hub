'use strict';

angular.module('welcome')
    .run(Run);

function Run(routerHelper) {
    routerHelper.configureStates(getEntryStates(), '/welcome');
}

function getEntryStates() {
    return [
        {
            state: 'welcome',
            config: {
                controller: welcomeController,
                templateUrl: 'js/welcome/welcome.html',
                url: '/welcome'
            }
        }
    ];
}